import java.util.*;

public class OX {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int count = 0;
        int n1,n2;
        char [][] arr = new char[3][3];
        arr[0][0] = '-';
	arr[0][1] = '-';
	arr[0][2] = '-';
	arr[1][0] = '-';
	arr[1][1] = '-';
	arr[1][2] = '-';
	arr[2][0] = '-';
	arr[2][1] = '-';
	arr[2][2] = '-';
        
        System.out.println("Welcome to OX Game");
        System.out.println(" 1 2 3");
        for(int i = 0; i < 3; i++){
            System.out.print(i+1);
            for(int j=0; j<3; j++){
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("X turn");
        
        try{
            while(count<9){
            if(count%2==0){
                System.out.println("Please input Row Col : ");
                n1 = kb.nextInt();
                n2 = kb.nextInt();
                if(arr[n1-1][n2-1]=='-'){
                    arr[n1-1][n2-1] = 'X';
                }else{
                    System.out.println("Error");
                    System.out.println("Try Again");
                    break;
                }
                System.out.println(" 1 2 3");
                for(int i = 0; i < 3; i++){
                    System.out.print(i+1);
                for(int j=0; j<3; j++){
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println("");               
                }
                System.out.println("O turn");
            }
            if(count%2!=0){
                System.out.println("Please input Row Col : ");
                n1 = kb.nextInt();
                n2 = kb.nextInt();
                if(arr[n1-1][n2-1]=='-'){
                    arr[n1-1][n2-1] = 'O';
                }else{
                    System.out.println("Error");
                    System.out.println("Try Again");
                    break;
                }                
                System.out.println(" 1 2 3");
                for(int i = 0; i < 3; i++){
                    System.out.print(i+1);
                for(int j=0; j<3; j++){
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println("");
                }
                System.out.println("X turn");
            }
            
            if(arr[0][0]==('X') && arr[0][1]==('X') && arr[0][2]==('X')
                    || arr[1][0]==('X') && arr[1][1]==('X') && arr[1][2]==('X')
                    || arr[2][0]==('X') && arr[2][1]==('X') && arr[2][2]==('X')
                    || arr[0][0]==('X') && arr[1][0]==('X') && arr[2][0]==('X')
                    || arr[0][1]==('X') && arr[1][1]==('X') && arr[2][1]==('X')
                    || arr[0][2]==('X') && arr[1][2]==('X') && arr[2][2]==('X')
                    || arr[0][0]==('X') && arr[1][1]==('X') && arr[2][2]==('X')
                    || arr[0][2]==('X') && arr[1][1]==('X') && arr[2][0]==('X')){
                System.out.println("Player X win...");
                break;
            }   
            if(arr[0][0]==('O') && arr[0][1]==('O') && arr[0][2]==('O')
                    || arr[1][0]==('O') && arr[1][1]==('O') && arr[1][2]==('O')
                    || arr[2][0]==('O') && arr[2][1]==('O') && arr[2][2]==('O')
                    || arr[0][0]==('O') && arr[1][0]==('O') && arr[2][0]==('O')
                    || arr[0][1]==('O') && arr[1][1]==('O') && arr[2][1]==('O')
                    || arr[0][2]==('O') && arr[1][2]==('O') && arr[2][2]==('O')
                    || arr[0][0]==('O') && arr[1][1]==('O') && arr[2][2]==('O')
                    || arr[0][2]==('O') && arr[1][1]==('O') && arr[2][0]==('O')){
                System.out.println("Player O win...");
                break;
            }
            if(count==9){
                System.out.println("draw");
            }
            
            count++;
        }
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("ArrayIndexOutOfBoundsException");
            System.out.println("Try Again");
        }
        
        System.out.println("Bye bye...");
        
        
    }
}
